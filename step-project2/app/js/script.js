
let isMenuOpened = false;

const openMenu = () => {
    isMenuOpened = true;
    document.getElementById('burger-menu').classList.remove('hidden');
    document.getElementsByClassName('header--button')[0].classList.add('hidden');
    document.getElementsByClassName('header--button--press')[0].classList.remove('hidden');
}

const closeMenu = () => {
    isMenuOpened = false;
    document.getElementById('burger-menu').classList.add('hidden');
    document.getElementsByClassName('header--button--press')[0].classList.add('hidden');
    document.getElementsByClassName('header--button')[0].classList.remove('hidden');

}


document.getElementsByClassName('header--button')[0].addEventListener('click', () => {
    openMenu();
})

document.getElementsByClassName('header--button--press')[0].addEventListener('click', () => {
    closeMenu();
})


const closeList = () =>{
    document.getElementById('hidden-list').classList.add('hidden');
    document.getElementById('page-footer').classList.add('hidden');
}

const openList = () =>{
    document.getElementById('hidden-list').classList.remove('hidden');
    document.getElementById('page-footer').classList.remove('hidden');
}

let isContentPresent = true;

document.getElementsByClassName('welcome-block--button')[0].addEventListener('click', () => {
    console.log('isContentPresent = ', isContentPresent)
    if (isContentPresent) {
        closeList()
        isContentPresent = false
    } else {
        isContentPresent = true
        openList()
    }
})



