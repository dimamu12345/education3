const button = document.getElementById('load-more-pictures-btn')

const hiddenClassName = 'hidden';

button.addEventListener("click", loadMore)

function loadMore() {
    let newImg = document.getElementById('ul-img-all2')
    newImg.classList.remove(hiddenClassName)
    button.classList.add(hiddenClassName);
}

const activeBtnClassName = 'active';
let shownWorks = 'all';
let previousClickedBtn = document.querySelector('.works-list-btn.active');
const worksListBtn = document.getElementsByClassName('works-list-btn');

for (const button of worksListBtn) {
    button.addEventListener('click', handleWorksBtnClick)
}

function handleWorksBtnClick(event) {
    previousClickedBtn.classList.remove(activeBtnClassName)
    document.getElementById(shownWorks).classList.add(hiddenClassName);

    document.getElementById(event.target.dataset.sectionName).classList.remove(hiddenClassName);
    event.target.classList.add(activeBtnClassName)

    shownWorks = event.target.dataset.sectionName;
    previousClickedBtn = event.target;
}

const activeBtnName = 'active-button';

let shownContent = 'our-web-design'
let earlyClickedBtn = document.querySelector('.our-services-button.active-button');
let triangleDown = getTriangle('our-web-design');
console.log(triangleDown)
const ListBtn = document.getElementsByClassName('our-services-button');

for (const button of ListBtn) {
    button.addEventListener('click', btnClick)
}

function btnClick(event) {
    earlyClickedBtn.classList.remove(activeBtnName);
    triangleDown.classList.add(hiddenClassName);
    document.getElementById(shownContent).classList.add(hiddenClassName);

    document.getElementById(event.target.dataset.sectionName).classList.remove(hiddenClassName);
    event.target.classList.add(activeBtnName)
    const nextTriangle = getTriangle(event.target.dataset.sectionName);
    nextTriangle.classList.remove(hiddenClassName);

    shownContent = event.target.dataset.sectionName;
    earlyClickedBtn = event.target;
    triangleDown = nextTriangle;
}

function getTriangle(att) {
    return document.querySelector(`.triangle-down[data-section-name="${att}"]`);
}

function queryElementByDataAtt(att) {
    return document.querySelector(`[data-section-name="${att}"]`);
}

////////////

const selectedPersonImg = document.querySelector('.person-prview-image')
const selectedPersonName = document.querySelector('.person-prview-name')
const selectedItemClassName = 'selected-slider-item';
let prevSelectedSliderItem = document.querySelector(`.${selectedItemClassName}`);

document.querySelectorAll('.slide-image').forEach(el => el.addEventListener('click', personSelected));

function personSelected(event) {
    prevSelectedSliderItem.classList.remove(selectedItemClassName);
    selectedPersonImg.src = event.target.src;
    event.target.classList.add(selectedItemClassName);
    prevSelectedSliderItem = event.target;
    selectedPersonName.innerHTML = event.target.dataset.personName;
}

var carousel = document.querySelector('.carousel');
var carouselContent = document.querySelector('.carousel-content');
var slides = document.querySelectorAll('.slide');

var arrayOfSlides = Array.prototype.slice.call(slides);
var carouselDisplaying;
var screenSize;
setScreenSize();
var lengthOfSlide;

function addClone() {
    var lastSlide = carouselContent.lastElementChild.cloneNode(true);
    lastSlide.style.left = (-lengthOfSlide) + "px";
    carouselContent.insertBefore(lastSlide, carouselContent.firstChild);
}

function removeClone() {
    var firstSlide = carouselContent.firstElementChild;
    firstSlide.parentNode.removeChild(firstSlide);
}

function moveSlidesRight() {
    var slides = document.querySelectorAll('.slide');
    var slidesArray = Array.prototype.slice.call(slides);
    var width = 0;

    slidesArray.forEach(function(el, i) {
        el.style.left = width + "px";
        width += lengthOfSlide;
    });
    addClone();
}
moveSlidesRight();

function moveSlidesLeft() {
    var slides = document.querySelectorAll('.slide');
    var slidesArray = Array.prototype.slice.call(slides);
    slidesArray = slidesArray.reverse();
    var maxWidth = (slidesArray.length - 1) * lengthOfSlide;

    slidesArray.forEach(function(el, i) {
        maxWidth -= lengthOfSlide;
        el.style.left = maxWidth + "px";
    });
}

function setScreenSize() {
    if (window.innerWidth >= 500) {
        carouselDisplaying = 4;
    } else if (window.innerWidth >= 300) {
        carouselDisplaying = 2;
    } else {
        carouselDisplaying = 1;
    }
    getScreenSize();
}

setScreenSize();

function getScreenSize() {
    var slides = document.querySelectorAll('.slide');
    var slidesArray = Array.prototype.slice.call(slides);
    lengthOfSlide = (carousel.offsetWidth / carouselDisplaying);
    var initialWidth = -lengthOfSlide;
    slidesArray.forEach(function(el) {
        el.style.width = lengthOfSlide + "px";
        el.style.left = initialWidth + "px";
        initialWidth += lengthOfSlide;
    });
}


var rightNav = document.querySelector('.nav-right');
rightNav.addEventListener('click', moveLeft);

var moving = true;

function moveRight() {
    debugger;
    if (moving) {
        moving = false;
        var lastSlide = carouselContent.lastElementChild;
        lastSlide.parentNode.removeChild(lastSlide);
        carouselContent.insertBefore(lastSlide, carouselContent.firstChild);
        removeClone();
        var firstSlide = carouselContent.firstElementChild;
        firstSlide.addEventListener('transitionend', activateAgain);
        firstSlide.querySelector('img').addEventListener('click', personSelected);
        moveSlidesRight();
    }
}

function activateAgain() {
    var firstSlide = carouselContent.firstElementChild;
    moving = true;
    firstSlide.removeEventListener('transitionend', activateAgain);
}

var leftNav = document.querySelector('.nav-left');
leftNav.addEventListener('click', moveRight);

function moveLeft() {
    if (moving) {
        moving = false;
        removeClone();
        var firstSlide = carouselContent.firstElementChild;
        moveSlidesLeft();
        firstSlide.addEventListener('transitionend', replaceToEnd);
    }
}

function replaceToEnd() {
    var firstSlide = carouselContent.firstElementChild;
    firstSlide.parentNode.removeChild(firstSlide);
    carouselContent.appendChild(firstSlide);
    firstSlide.style.left = ((arrayOfSlides.length - 1) * lengthOfSlide) + "px";
    addClone();
    moving = true;
    firstSlide.removeEventListener('transitionend', replaceToEnd);
}

var initialX;
var initialPos;

function slightMove(e) {
    if (moving) {
        var movingX = e.clientX;
        var difference = initialX - movingX;
        if (Math.abs(difference) < (lengthOfSlide / 4)) {
            slightMoveSlides(difference);
        }
    }
}

function getInitialPos() {
    var slides = document.querySelectorAll('.slide');
    var slidesArray = Array.prototype.slice.call(slides);
    initialPos = [];
    slidesArray.forEach(function(el) {
        var left = Math.floor(parseInt(el.style.left.slice(0, -2)));
        initialPos.push(left);
    });
}

function slightMoveSlides(newX) {
    var slides = document.querySelectorAll('.slide');
    var slidesArray = Array.prototype.slice.call(slides);
    slidesArray.forEach(function(el, i) {
        var oldLeft = initialPos[i];
        el.style.left = (oldLeft + newX) + "px";
    });
}