
import './App.css';

import './components/Button/Button.css'
import { Button } from './components/Button/Button'
import { Modal } from './components/Modal/Modal';
import React, { useState } from 'react'

function App() {
    const nameFirstButton = 'First button';
    const nameSecondButton = 'Second button';
    const nameFirstModal = 'First Modal';
    const nameSecondModal = 'Second Modal';

    const [showFirstModal, setShowFirstModal] = useState(false);
    const [showSecondModal, setShowSecondModal] = useState(false);

  return (
    <div className="App">
        <Button backgroundColor="blue" text={nameFirstButton} onButtonClick={()=> setShowFirstModal(true)}></Button>
        <Button backgroundColor="green" text={nameSecondButton} onButtonClick={()=> setShowSecondModal(true)}></Button>
        <Modal show={showFirstModal} setShow={setShowFirstModal} text={nameFirstModal}></Modal>
        <Modal show={showSecondModal} setShow={setShowSecondModal} text={nameSecondModal}></Modal>
    </div>
  );
}

export default App;
