import React, {useState} from 'react';

import "./CardProduct.css";

import {Button} from '../Button/Button';
import {Modal} from '../Modal/Modal';

const CardProduct = ({item, onItemAddedToCard, onFavoritesSelect, isFavorite}) => {
    const [modalShown, setModalShown] = useState(false);

    const openAddToCardModal = () => {
        setModalShown(true);
    }

    const itemAdded = () => {
        onItemAddedToCard(item.name);
        setModalShown(false);
    }

    return (
        <div className="cardProduct">
            <div className="cardProduct-img"><img src={item.url}/></div>
            <div className="cardProduct-name">{item.name}</div>
            <div className="cardProduct-price">{item.price} </div>
            <div className="cardProduct-itemNumber">{item.itemNumber}</div>
            <div className="cardProduct-color">{item.color}</div>
            <div className="cardProduct-icons">
                <button onClick={() => onFavoritesSelect(item.name)}>
                    {
                        isFavorite ?
                            <img
                                src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Heart_coraz%C3%B3n.svg/2048px-Heart_coraz%C3%B3n.svg.png"
                                alt=""/>
                            : <img src="https://www.svgrepo.com/show/163334/heart.svg" alt=""/>

                    }
                </button>
            </div>
            <Button text="Add to cart" onButtonClick={openAddToCardModal}></Button>
            <Modal show={modalShown} setShow={setModalShown} onOk={itemAdded}
                   text={`Ви хочете додати товар ${item.name} в кошик?`}></Modal>

        </div>

    )
}

export {CardProduct};