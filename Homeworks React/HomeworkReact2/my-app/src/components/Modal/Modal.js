import './Modal.css'
import React, {useEffect, useRef} from 'react'
import {Button} from '../Button/Button';

const Modal = ({show, setShow, onOk, text}) => {
    const ref = useRef()

    const checkIfClickedOutside = e => {
        if (ref.current && !ref.current.contains(e.target)) {
            setShow(false);
        }
    }

    useEffect(() => {
        document.addEventListener("mousedown", checkIfClickedOutside)
        return () => {
            document.removeEventListener("mousedown", checkIfClickedOutside)
        }
    }, []);

    return (
        show && (
            <div className="modal-background-color">
                <div className="modal" ref={ref}>
                    <div className="modal-header">
                        <h4>{text}</h4>
                    </div>
                    <div className="modal-content">
                        <div className="input">
                            <input className="input-box" type="text"/>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="button-wrapper">
                            <Button text="OK"  onButtonClick={onOk}></Button>
                            <Button text="Cancel" onButtonClick={() => setShow(false)}></Button>
                        </div>
                    </div>
                </div>
            </div>
        )
    )
};


export {Modal};

