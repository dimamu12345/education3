const Button = (props) => {
    const {backgroundColor, onButtonClick, text} = props;
    const styles = {
        backgroundColor: backgroundColor
    };

    return (
        <button
            style={styles}
            onClick={onButtonClick}
        >
            {text}
        </button>
    )
};

export {Button};

