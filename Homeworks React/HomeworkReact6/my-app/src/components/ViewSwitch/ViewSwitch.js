import React, { useContext } from 'react';
import { ShopContext } from '../../context';

function ViewSwitch() {
    const { view, setView } = useContext(ShopContext);

    return (
        <div className="view-switch">
            <button onClick={() => setView('all-product-card')} className={view === 'all-product-card' ? 'active' : ''}>Card</button>
            <button onClick={() => setView('all-product-table')} className={view === 'all-product-table' ? 'active' : ''}>Table</button>
        </div>
    );
}

export default ViewSwitch;
