import React from 'react';
import { Formik, Form, useField } from 'formik';
import * as Yup from 'yup';
import './Form.css'
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {removeAllItems} from "../../store/reducer";

const MyTextInput = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
        <>
            <label htmlFor={props.id || props.name}>{label}</label>
            <input className="text-input" {...field} {...props} />
            {meta.touched && meta.error ? (
                <div className="error">{meta.error}</div>
            ) : null}
        </>
    );
};

const MyCheckbox = ({ children, ...props }) => {
    const [field, meta] = useField({ ...props, type: 'checkbox' });
    return (
        <div>
            <label className="checkbox-input">
                <input type="checkbox" {...field} {...props} />
                {children}
            </label>
            {meta.touched && meta.error ? (
                <div className="error">{meta.error}</div>
            ) : null}
        </div>
    );
};

const MySelect = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
        <div>
            <label htmlFor={props.id || props.name}>{label}</label>
            <select {...field} {...props} />
            {meta.touched && meta.error ? (
                <div className="error">{meta.error}</div>
            ) : null}
        </div>
    );
};



const SignupForm = () => {
    const dispatch = useDispatch();

    const userBasket = useSelector((state) => state.shopItems.userBasket, shallowEqual)

    const removeAllItemBasket = () => {
        console.log('userBasket = ', userBasket)
        dispatch(removeAllItems())
    }

    return (
        <>
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    age: '',
                    acceptedTerms: false,
                    phoneNumber: '',
                    residentialAddress: '',
                }}
                validationSchema={Yup.object({
                    firstName: Yup.string()
                        .max(15, 'Must be 15 characters or less')
                        .required('Required'),
                    lastName: Yup.string()
                        .max(20, 'Must be 20 characters or less')
                        .required('Required'),
                    age: Yup.number()
                        .min(0, 'Age is incorrect')
                        .max(100,  'Age is incorrect' )
                        .required('Required'),
                    acceptedTerms: Yup.boolean()
                        .required('Required')
                        .oneOf([true], 'You must accept the terms and conditions.'),
                    phoneNumber: Yup.string()
                        .min(10, 'Must be at least 10 digits')
                        .max(15,  'Must be no more than 15 digits' )
                        .required('Required'),
                    residentialAddress: Yup.string()
                        .max(44,'Must be 44 characters or less' )
                        .required('Required'),
                })}
                onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                        // alert(JSON.stringify(values, null, 2));
                        setSubmitting(false);
                        console.log(JSON.stringify(values, null, 2))
                    }, 400);
                }}
            >
                <Form className="form-basket">
                    <MyTextInput
                        label="First Name"
                        name="firstName"
                        type="text"
                        placeholder="Name"
                    />

                    <MyTextInput
                        label="Last Name"
                        name="lastName"
                        type="text"
                        placeholder="Last Name"
                    />

                    <MyTextInput
                        label="Age"
                        name="age"
                        type="number"
                        placeholder="Age"
                    />
                    <MyTextInput
                        label="Residential Address"
                        name="residentialAddress"
                        type="text"
                        placeholder="Residential Address"
                    />
                    <MyTextInput
                        label="Phone Number"
                        name="phoneNumber"
                        type="text"
                        placeholder="+38 (093) 333 33 33"
                    />


                    <MyCheckbox name="acceptedTerms">
                        I accept the terms and conditions
                    </MyCheckbox>

                    <button onClick={removeAllItemBasket} type="submit">Submit</button>
                </Form>
            </Formik>
        </>
    );
};

export default SignupForm