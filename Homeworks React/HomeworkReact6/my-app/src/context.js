import React, { createContext, useState } from 'react';

export const ShopContext = createContext();

function  ShopContextProvider(props) {
    const [view, setView] = useState('all-product-card');

    return (
        <ShopContext.Provider value={{ view, setView }}>
            {props.children}
        </ShopContext.Provider>
    );
}

export default ShopContextProvider;
