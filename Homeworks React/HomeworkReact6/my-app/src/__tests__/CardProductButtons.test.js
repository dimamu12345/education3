import { render, screen } from '@testing-library/react';
import {Button} from "../components/Button/Button";
import {Modal} from "../components/Modal/Modal";

describe('Product details', () => {
    it('should render the Add to cart button and the Modal component when onItemAddedToCard is true', () => {
        const item = { id: 1, name: 'Product 1', price: 10 };
        const onFavoritesSelect = jest.fn();
        const openAddToCardModal = jest.fn();
        const setModalShown = jest.fn();
        const itemAdded = jest.fn();

        render(
            <>
                <button onClick={() => onFavoritesSelect(item)}>
                    <img
                        src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Heart_coraz%C3%B3n.svg/2048px-Heart_coraz%C3%B3n.svg.png"
                        alt=""
                    />
                </button>

                <Button text="Add to cart" onButtonClick={openAddToCardModal} />
                <Modal
                    show={true}
                    setShow={setModalShown}
                    onOk={itemAdded}
                    text={`Ви хочете додати товар ${item.name} в кошик?`}
                />
            </>
        );

        expect(screen.getByRole('button', { name: 'Add to cart' })).toBeInTheDocument();
        expect(screen.getByText(`Ви хочете додати товар ${item.name} в кошик?`)).toBeInTheDocument();
    });

    it('should not render the Add to cart button and the Modal component when onItemAddedToCard is false', () => {
        const item = { id: 1, name: 'Product 1', price: 10 };
        const onFavoritesSelect = jest.fn();
        const onItemAddedToCard = false;
        const openAddToCardModal = jest.fn();
        const setModalShown = jest.fn();
        const itemAdded = jest.fn();

        render(
            <>
                <button onClick={() => onFavoritesSelect(item)}>
                    <img
                        src="https://www.svgrepo.com/show/163334/heart.svg"
                        alt=""
                    />
                </button>

                {onItemAddedToCard && (
                    <Button text="Add to cart" onButtonClick={openAddToCardModal} />
                )}
                <Modal
                    show={false}
                    setShow={setModalShown}
                    onOk={itemAdded}
                    text={`Ви хочете додати товар ${item.name} в кошик?`}
                />
            </>
        );

        expect(screen.queryByRole('button', { name: 'Add to cart' })).not.toBeInTheDocument();
        expect(screen.queryByText(`Ви хочете додати товар ${item.name} в кошик?`)).not.toBeInTheDocument();
    });
});
