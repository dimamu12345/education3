import React from 'react';
import renderer from 'react-test-renderer';
import {CardProduct} from '../components/CardProduct/CardProduct';

describe('CardProduct component', () => {
    it('matches the snapshot', () => {
        const item = {
            url: 'http://example.com/image.png',
            name: 'Product name',
            price: '$9.99',
            itemNumber: '12345',
            color: 'red',
        };
        const onItemAddedToCard = jest.fn();
        const onItemRemoveToCard = jest.fn();
        const onFavoritesSelect = jest.fn();
        const isFavorite = true;
        const tree = renderer.create(<CardProduct
            item={item}
            onItemAddedToCard={onItemAddedToCard}
            onItemRemoveToCard={onItemRemoveToCard}
            onFavoritesSelect={onFavoritesSelect}
            isFavorite={isFavorite}
        />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
