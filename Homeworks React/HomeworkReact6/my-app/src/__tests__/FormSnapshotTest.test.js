import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureMockStore from 'redux-mock-store';
import SignupForm from '../components/Form/Form';

const mockStore = configureMockStore([]);

describe('SignupForm', () => {
    it('should render correctly', () => {
        const store = mockStore({
            shopItems: {
                userBasket: []
            }
        });
        const tree = renderer.create(
            <Provider store={store}>
                <SignupForm />
            </Provider>
        ).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
