import React from 'react';
import renderer from 'react-test-renderer';
import { Modal } from '../components/Modal/Modal';

describe('Modal', () => {
    it('should render correctly when show is true', () => {
        const component = renderer.create(
            <Modal show={true} setShow={() => {}} onOk={() => {}} text="Test Modal" />
        );
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('should render correctly when show is false', () => {
        const component = renderer.create(
            <Modal show={false} setShow={() => {}} onOk={() => {}} text="Test Modal" />
        );
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});
