import { render, fireEvent } from '@testing-library/react';

describe('removeAllItemBasket button', () => {
    it('calls the onClick function when clicked', () => {
        const mockOnClick = jest.fn();
        const { getByText } = render(<button onClick={mockOnClick}>Submit</button>);
        const button = getByText('Submit');
        fireEvent.click(button);
        expect(mockOnClick).toHaveBeenCalledTimes(1);
    });
});
