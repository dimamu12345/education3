import shopItemsReducer, {
    getShopItemsSuccess,
    addItemToUserBasket,
    changeUserFavorites,
    removeItemToUserFavorites,
    removeItemToUserBasket,
    removeAllItems,
} from '../store/reducer';

describe('shopItemsReducer', () => {
    const initialState = {
        items: [],
        userBasket: [],
        userFavorites: [],
    };

    it('should handle getShopItemsSuccess', () => {
        const mockPayload = {
            loadedItems: [
                { name: 'Item 1', price: 10 },
                { name: 'Item 2', price: 20 },
            ],
        };
        const newState = shopItemsReducer(initialState, getShopItemsSuccess(mockPayload));

        expect(newState.items).toEqual(mockPayload.loadedItems);
    });

    it('should handle addItemToUserBasket', () => {
        const mockPayload = { newItem: { name: 'Item 1', price: 10 } };
        const newState = shopItemsReducer(initialState, addItemToUserBasket(mockPayload));

        expect(newState.userBasket).toContain(mockPayload.newItem);
    });

    it('should handle changeUserFavorites', () => {
        const mockPayload = { selectedItem: { name: 'Item 1', price: 10 } };
        const newState = shopItemsReducer(initialState, changeUserFavorites(mockPayload));

        expect(newState.userFavorites).toContain(mockPayload.selectedItem);
    });

    it('should handle removeItemToUserFavorites', () => {
        const mockPayload = { removedItem: { name: 'Item 1', price: 10 } };
        const newState = shopItemsReducer(
            { ...initialState, userFavorites: [mockPayload.removedItem] },
            removeItemToUserFavorites(mockPayload),
        );

        expect(newState.userFavorites).not.toContain(mockPayload.removedItem);
    });

    it('should handle removeItemToUserBasket', () => {
        const mockPayload = { removedItem: { name: 'Item 1', price: 10 } };
        const newState = shopItemsReducer(
            { ...initialState, userBasket: [mockPayload.removedItem] },
            removeItemToUserBasket(mockPayload),
        );

        expect(newState.userBasket).not.toContain(mockPayload.removedItem);
    });

    it('should handle removeAllItems', () => {
        const newState = shopItemsReducer(
            { ...initialState, userBasket: [{ name: 'Item 1', price: 10 }] },
            removeAllItems(),
        );

        expect(newState.userBasket).toEqual([]);
    });
});
