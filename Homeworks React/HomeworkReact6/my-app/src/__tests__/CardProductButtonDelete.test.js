import {render, fireEvent, getByText} from "@testing-library/react";
import {Button} from "../components/Button/Button";

describe("Button component", () => {
    test("renders 'Remove' button when onItemRemoveToCard is truthy", () => {
        const onItemRemoveToCard = true;
        const itemRemove = jest.fn();
        const { getByText } = render(
            <Button text="Remove" onButtonClick={itemRemove} />,
            { onItemRemoveToCard }
        );
        const removeButton = getByText("Remove");
        expect(removeButton).toBeInTheDocument();
    });

    test("calls itemRemove function when 'Remove' button is clicked", () => {
        const onItemRemoveToCard = true;
        const itemRemove = jest.fn();
        const { getByText } = render(
            <Button text="Remove" onButtonClick={itemRemove} />,
            { onItemRemoveToCard }
        );
        const removeButton = getByText("Remove");
        fireEvent.click(removeButton);
        expect(itemRemove).toHaveBeenCalledTimes(1);
    });
});
