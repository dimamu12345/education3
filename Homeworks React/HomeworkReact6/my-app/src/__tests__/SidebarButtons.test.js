import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import {NavLink} from "react-router-dom";

describe('NavLink component', () => {
    test('renders with correct text and href for Home link', () => {
        render(
            <MemoryRouter>
                <NavLink to="/" className="nav-link button">
                    <button className='sidebar-button-home sidebar-button'>Home</button>
                </NavLink>
            </MemoryRouter>
        );
        const linkElement = screen.getByRole('link', { name: /Home/i });
        expect(linkElement).toHaveAttribute('href', '/');
        expect(linkElement.textContent).toBe('Home');
    });

    test('renders with correct text and href for Basket link', () => {
        render(
            <MemoryRouter>
                <NavLink to="/basket" className="nav-link button">
                    <button className='sidebar-button-basket sidebar-button'>Basket</button>
                </NavLink>
            </MemoryRouter>
        );
        const linkElement = screen.getByRole('link', { name: /Basket/i });
        expect(linkElement).toHaveAttribute('href', '/basket');
        expect(linkElement.textContent).toBe('Basket');
    });

    test('renders with correct text and href for Favorites link', () => {
        render(
            <MemoryRouter>
                <NavLink to="/favorites" className="nav-link button">
                    <button className='sidebar-button-favorites sidebar-button'>Favorites</button>
                </NavLink>
            </MemoryRouter>
        );
        const linkElement = screen.getByRole('link', { name: /Favorites/i });
        expect(linkElement).toHaveAttribute('href', '/favorites');
        expect(linkElement.textContent).toBe('Favorites');
    });
});
