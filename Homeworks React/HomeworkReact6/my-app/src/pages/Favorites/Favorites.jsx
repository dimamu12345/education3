import {CardProduct} from "../../components/CardProduct/CardProduct";
import {useSelector, shallowEqual, useDispatch} from "react-redux";
import {removeItemToUserFavorites} from "../../store/reducer";

const Favorites = () => {

    const dispatch = useDispatch()
    const addCardToFavorites = useSelector((state) => state.shopItems.userFavorites, shallowEqual)
    const removeCard = (item) => {
        dispatch(removeItemToUserFavorites({removedItem: item}))
    }

    return (
        <>
            <div><h1 className="basket-name">Your Favorites</h1></div>
            <div>
                {
                    addCardToFavorites.length ? (
                        addCardToFavorites.map(item => {
                            return <CardProduct
                                item={item}
                                onItemRemoveToCard={removeCard}
                            ></CardProduct>
                        })
                    ) : (
                        <span className="basket-name">Sorry your favorites is empty</span>
                    )
                }
            </div>
        </>
    )
}

export {Favorites};