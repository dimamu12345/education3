import './App.css';
import './components/Button/Button.css'
import Sidebar from "./components/Sidebar";
import RootRouters from "./routes"
import ShopContextProvider from "../src/context";

export default function App() {

    return (
        <ShopContextProvider>
            <div className="App">
                <Sidebar/>
                <RootRouters/>
            </div>
        </ShopContextProvider>
    );
}

export {App};
