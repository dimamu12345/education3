import {configureStore} from "@reduxjs/toolkit";

import shopItems from './reducer';

const store = configureStore({
        reducer: {
            shopItems: shopItems
        }
    });

export default store;
