import {Route, Routes} from "react-router-dom";
import HomePage from "../pages/HomePage/HomePage";
import {Basket} from "../pages/Basket/Basket";
import {Favorites} from "../pages/Favorites/Favorites";
import NotPage from "../pages/NotPage/NotPage";

export default function RootRouters() {
    return (
        <Routes>
            <Route path="/" element={<HomePage />}/>
            <Route path="basket" element={<Basket/>}/>
            <Route path="favorites" element={<Favorites/>}/>
            <Route path="*" element={<NotPage/>}/>
        </Routes>
    )
}

export {RootRouters}