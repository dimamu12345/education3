import '../../App.css';
import '../../components/Button/Button.css'
import {Modal} from '../../components/Modal/Modal';
import React, {useEffect, useState} from 'react'
import {CardProduct} from '../../components/CardProduct/CardProduct';
import Sidebar from "../../components/Sidebar";
import {fetchData, addItemToUserBasket, changeUserFavorites} from "../../store/reducer";
import {useDispatch, useSelector, shallowEqual} from "react-redux";


function App() {
    const nameFirstModal = 'First Modal';
    const nameSecondModal = 'Second Modal';
    const [showFirstModal, setShowFirstModal] = useState(false);
    const [showSecondModal, setShowSecondModal] = useState(false);
    const dispatch = useDispatch()

    useEffect(() => {
       dispatch(fetchData())
    }, []);

    const items = useSelector((state) => state.shopItems.items, shallowEqual)
    let userBasket = useSelector((state) => state.shopItems.userBasket, shallowEqual)
    const handleItemAddedToCard = (selectedItem) => {
        dispatch(addItemToUserBasket({newItem: selectedItem}));
    }

    const userFavorites = useSelector((state) => state.shopItems.userFavorites, shallowEqual)
    const handleFavoritesSelect = (item) => {
        dispatch(changeUserFavorites({selectedItem: item}))
    }

    const isSelectedAsFavorite = (name) => {
        return userFavorites.find(favorite => favorite.name === name);
    }

    return (
        <div className="App">
            <Sidebar/>

            <div>
                <img style={{width: '20px'}}
                     src="https://cdn.icon-icons.com/icons2/1580/PNG/96/2849824-basket-buy-market-multimedia-shop-shopping-store_107977.png"
                     alt=""/> {userBasket.length}
                <img style={{width: '20px'}}
                     src="https://cdn.icon-icons.com/icons2/38/PNG/96/like_favorite_heart_5759.png"
                     alt=""/> {userFavorites.length}
            </div>

            <Modal show={showFirstModal} setShow={setShowFirstModal} text={nameFirstModal}></Modal>
            <Modal show={showSecondModal} setShow={setShowSecondModal} text={nameSecondModal}></Modal>
            <div className="all-product">
                {
                    items.map(item => {
                        return <CardProduct
                            item={item}
                            isFavorite={isSelectedAsFavorite(item.name)}
                            onItemAddedToCard={handleItemAddedToCard}
                            onFavoritesSelect={handleFavoritesSelect}
                        ></CardProduct>
                    })

                }
            </div>
        </div>
    );
}



export default App;
