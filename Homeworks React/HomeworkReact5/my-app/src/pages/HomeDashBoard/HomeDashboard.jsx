import { Link, NavLink } from "react-router-dom";
import {ReactComponent as Basket} from "./icons/basket-.svg";
import {ReactComponent as Favorites} from "./icons/favorites.svg";
import "./HomeDashboard.scss";
function HomeDashboard (){
    return (
        <div className="home-dashboard">
            <div className="container">
                <div className="dashboard-rooms">
                    <div className="room-card">
                        <Link to="basket" className="room-icon">
                            <Basket/>
                        </Link>
                    </div>
                    <div className="room-card">
                        <Link to="favorites" className="room-icon">
                            <Favorites/>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomeDashboard;