import {CardProduct} from "../../components/CardProduct/CardProduct";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {removeItemToUserBasket} from "../../store/reducer";
import Form from "../../components/Form/Form"


const Basket = () => {
    const dispatch = useDispatch()
    const addCardToBasket = useSelector((state) => state.shopItems.userBasket, shallowEqual)
    const removeCard = (item) => {
        dispatch(removeItemToUserBasket({removedItem: item}))
    }

    return (
        <>
            <div><h1 className="basket-name">Your basket</h1></div>
            <div className="all-product">
                {
                    addCardToBasket.length ? (
                        addCardToBasket.map(item => {
                            return <CardProduct
                                item={item}
                                onItemRemoveToCard={removeCard}
                            ></CardProduct>
                        })
                    ) : (
                        <span>Sorry your basket is empty</span>
                    )
                }
            </div>
            <Form />
        </>
    )
}

export {Basket};