import {Link} from "react-router-dom";
import "./NotPage.scss";
export default function NotPage () {
    return (
        <div className="widget-container not-page">
            <div>
                <p className="page-title">This page does not exist.</p>
                <p className="page-desc">Let’s head back home and try that again. The truth is out there…</p>
                <p>
                    <Link to="/">Home</Link>
                </p>
            </div>
        </div>)
}
export {NotPage};