import React, {useEffect, useState} from 'react'
import {CardProduct} from "../../components/CardProduct/CardProduct";


const Basket = () => {

    const [basketItems, setBasketItems] = useState([]);


    useEffect(() => {
        const userItems = JSON.parse(localStorage.getItem('user_selected_items'));

        setBasketItems(userItems?.userBasket ? userItems?.userBasket : []);
    }, []);

    const removeCard = (removedItem) => {
        setBasketItems(basketItems.filter(basketItem => basketItem.name !== removedItem.name))
        localStorage.setItem('user_selected_items', JSON.stringify({userBasket: basketItems.filter(basketItem => basketItem.name !== removedItem.name)}));
    }

    return (
        <>
            <div><h1 className="basket-name">Your basket</h1></div>
            <div className="all-product">
                {
                    basketItems.length ? (
                        basketItems.map(item => {
                            return <CardProduct
                                item={item}
                                onItemRemoveToCard={removeCard}
                            ></CardProduct>
                        })
                    ) : (
                        <span>Sorry your basket is empty</span>
                    )
                }
            </div>
        </>
    )
}

export {Basket};