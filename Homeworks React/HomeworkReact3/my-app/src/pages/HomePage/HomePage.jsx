import '../../App.css';


import '../../components/Button/Button.css'
import {Modal} from '../../components/Modal/Modal';
import React, {useEffect, useState} from 'react'
import fakeApiResponse from '../../arrProduct.json';
import {CardProduct} from '../../components/CardProduct/CardProduct';
import Sidebar from "../../components/Sidebar";



function App() {
    const nameFirstModal = 'First Modal';
    const nameSecondModal = 'Second Modal';

    const [showFirstModal, setShowFirstModal] = useState(false);
    const [showSecondModal, setShowSecondModal] = useState(false);
    const [itemsList, setItemsList] = useState([]);
    const [userBasket, setUserBasket] = useState([]);
    const [userFavorites, setUserFavorites] = useState([]);

    const fetchData = () => {
        // simulate api call
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(fakeApiResponse);
            }, 1000)
        })
    }

    useEffect(() => {
        fetchData().then(
            response => setItemsList(response)
        )
    }, []);

    const handleItemAddedToCard = (itemName) => {
        setUserBasket([...userBasket, itemName]);
    }

    // const onItemRemoveToCard = (

    const handleFavoritesSelect = (itemName) => {
        if (isSelectedAsFavorite(itemName)) {
            setUserFavorites([...userFavorites.filter(item => item.name !== itemName)]);
        } else {
            setUserFavorites([...userFavorites, itemName]);
        }
    }

    const isSelectedAsFavorite = (name) => {
        return userFavorites.find(favorite => favorite.name === name);
    }

    useEffect(() => {
        const userState = {
            userFavorites, userBasket
        };
        localStorage.setItem('user_selected_items', JSON.stringify(userState));
    }, [userFavorites, userBasket])

    return (
        <div className="App">
            <Sidebar/>

            <div>
                <img style={{width: '20px'}}
                     src="https://cdn.icon-icons.com/icons2/1580/PNG/96/2849824-basket-buy-market-multimedia-shop-shopping-store_107977.png"
                     alt=""/> {userBasket.length}
                <img style={{width: '20px'}}
                     src="https://cdn.icon-icons.com/icons2/38/PNG/96/like_favorite_heart_5759.png"
                     alt=""/> {userFavorites.length}
            </div>

            <Modal show={showFirstModal} setShow={setShowFirstModal} text={nameFirstModal}></Modal>
            <Modal show={showSecondModal} setShow={setShowSecondModal} text={nameSecondModal}></Modal>
            <div className="all-product">
                {
                    itemsList.map(item => {
                        return <CardProduct
                            item={item}
                            isFavorite={isSelectedAsFavorite(item.name)}
                            onItemAddedToCard={handleItemAddedToCard}
                            onFavoritesSelect={handleFavoritesSelect}
                        ></CardProduct>
                    })

                }
            </div>
        </div>
    );
}



export default App;
