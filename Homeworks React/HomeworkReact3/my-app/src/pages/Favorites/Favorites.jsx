import React, {useEffect, useState} from 'react'
import {CardProduct} from "../../components/CardProduct/CardProduct";

const Favorites = () => {

    const [favoritesItems, setFavoritesItems] = useState([]);

    useEffect(() => {
        const userFavoritesItems = JSON.parse(localStorage.getItem('user_selected_items'));

        setFavoritesItems(userFavoritesItems?.userFavorites ? userFavoritesItems?.userFavorites : []);
    }, []);
    const removeCard = (removedItem) => {
        setFavoritesItems(favoritesItems.filter(favoritesItems => favoritesItems.name !== removedItem.name))
        localStorage.setItem('user_selected_items', JSON.stringify({userBasket: favoritesItems.filter(favoritesItems => favoritesItems.name !== removedItem.name)}));
    }

    return (
        <>
            <div><h1 className="basket-name">Your Favorites</h1></div>
            <div className="all-product">
                {
                    favoritesItems.length ? (
                        favoritesItems.map(item => {
                            return <CardProduct
                                item={item}
                                onItemRemoveToCard={removeCard}
                            ></CardProduct>
                        })
                    ) : (
                        <span>Sorry your favorites is empty</span>
                    )
                }
            </div>
        </>
    )
}

export {Favorites};