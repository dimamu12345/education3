import './App.css';


import './components/Button/Button.css'
import Sidebar from "./components/Sidebar";
import {Route, Routes} from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import {Basket} from './pages/Basket/Basket'
import {Favorites} from "./pages/Favorites/Favorites";
import {NotFoundPage} from "./pages/NotFoundPage/NotFoundPage";

function App() {

    return (
        <div className="App">
            <Sidebar/>

            <Routes>
                <Route path="/" element={<HomePage />}/>
                <Route path="basket" element={<Basket/>}/>
                <Route path="favorites" element={<Favorites/>}/>
                <Route path="" element={<NotFoundPage/>}/>
            </Routes>
        </div>
    );
}


export default App;
