import {Link, NavLink} from "react-router-dom";
import {ReactComponent as Home} from "./images/home.svg";
import {ReactComponent as Logo} from "./images/logo.svg";
import {ReactComponent as Weather} from "./images/weather.svg";

import "./Sidebar.scss";


function Sidebar() {
    return (<div className="g-sidebar">
        <NavLink to="/" activeClassName="active" className="nav-link button">
            <button className='sidebar-button-home sidebar-button'>Home</button>
        </NavLink>

        <NavLink to="/basket" activeClassName="active" className="nav-link button">
            <button className='sidebar-button-basket sidebar-button'>Basket</button>
        </NavLink>

        <NavLink to="/favorites" activeClassName="active" className="nav-link button">
            <button className='sidebar-button-favorites sidebar-button'>Favorites</button>
        </NavLink>

    </div>)
}

export default Sidebar;