import {NavLink} from "react-router-dom";
import "./Sidebar.scss";


function Sidebar() {
    return (<div className="g-sidebar">
        <NavLink to="/" className="nav-link button">
            <button className='sidebar-button-home sidebar-button'>Home</button>
        </NavLink>
        <NavLink to="/basket" className="nav-link button">
            <button className='sidebar-button-basket sidebar-button'>Basket</button>
        </NavLink>

        <NavLink to="/favorites" className="nav-link button">
            <button className='sidebar-button-favorites sidebar-button'>Favorites</button>
        </NavLink>

    </div>)
}

export default Sidebar;