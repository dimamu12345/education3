import {createSlice} from '@reduxjs/toolkit';
import fakeApiResponse from "../arrProduct.json";


export const fetchData = () => async (dispatch) => {
    setTimeout(() => {
        dispatch(getShopItemsSuccess({loadedItems: fakeApiResponse}));
    }, 1000)
}


const initialState = {
    userBasket: [],
    userFavorites: [],
    items: []
}


const shopItems = createSlice({
    name: 'shopItems',
    initialState,
    reducers: {
        getShopItemsSuccess(state, {payload}) {
            state.items = payload.loadedItems;
        },
        addItemToUserBasket(state, {payload}) {
            state.userBasket = [...state.userBasket, payload.newItem]
        },
        changeUserFavorites(state, {payload}) {
            if (state.userFavorites.find(favItem => favItem.name === payload.selectedItem.name)) {
                const changeFavorites = state.userFavorites.filter(favorite => favorite.name === payload.name);
                state.userFavorites = changeFavorites
            } else {
                state.userFavorites = [...state.userFavorites, payload.selectedItem]
            }
        },
        removeItemToUserFavorites(state, {payload}) {
            if (state.userFavorites.find(favorite => favorite.name === payload.removedItem.name)) {
                const userFavoritesArr = state.userFavorites.filter(favorite => favorite.name !== payload.removedItem.name)
                state.userFavorites = userFavoritesArr
            } else {
                state.userFavorites = [...state.userFavorites, payload.removedItem]
            }
        },
        removeItemToUserBasket(state, {payload}) {
            if (state.userBasket.find(basketItems => basketItems.name === payload.removedItem.name)) {
                const newBasketArr = state.userBasket.filter(basketItems => basketItems.name !== payload.removedItem.name)
                state.userBasket = newBasketArr
            } else {
                state.userBasket = [...state.userBasket, payload.removedItem]
            }
        }
    }
});

export const {
    getShopItemsSuccess,
    addItemToUserBasket,
    changeUserFavorites,
    removeItemToUserFavorites,
    removeItemToUserBasket,
} = shopItems.actions;

export default shopItems.reducer;
