import './App.css';
import './components/Button/Button.css'
import Sidebar from "./components/Sidebar";
import RootRouters from "./routes"

export default function App() {

    return (
        <div className="App">
                <Sidebar/>
                <RootRouters/>
        </div>
    );
}

export {App};
