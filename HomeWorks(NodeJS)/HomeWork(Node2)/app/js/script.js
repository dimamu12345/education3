
let isMenuOpened = false;

const openMenu = () => {
    isMenuOpened = true;
    document.getElementById('menu-list').classList.remove('hidden');
    document.getElementsByClassName('header--button')[0].classList.add('hidden');
    document.getElementsByClassName('header--button--press')[0].classList.remove('hidden');
}

const closeMenu = () => {
    isMenuOpened = false;
    document.getElementById('menu-list').classList.add('hidden');
    document.getElementsByClassName('header--button--press')[0].classList.add('hidden');
    document.getElementsByClassName('header--button')[0].classList.remove('hidden');

}


document.getElementsByClassName('header--button')[0].addEventListener('click', () => {
    openMenu();
})

document.getElementsByClassName('header--button--press')[0].addEventListener('click', () => {
    closeMenu();
})