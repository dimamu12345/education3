// Почнемо із ключового слова async. Воно ставиться перед функцією
//Слово async має один простий зміст: ця функція завжди повертає проміс. Значення інших типів обертаються в успішний проміс автоматично.




async function search() {
    let getIpUrl = 'https://api.ipify.org/?format=json';
    let getPhysicalIpUrl = 'http://ip-api.com/json/';

    let getIpResponse = await fetch(getIpUrl);
    let userIp = await getIpResponse.json();
    let getPhysicalIpResponse = await fetch(getPhysicalIpUrl + userIp.ip);
    let dataAboutUserIp = await getPhysicalIpResponse.json();
    renderUserData(dataAboutUserIp);
}

function handleClick() {
    search();
}

function renderUserData(dataAboutUserIp) {
    let list = document.createElement('ul')
    const timezone = document.createElement('li')
    timezone.append(`User timezone is = ${dataAboutUserIp.timezone}`)
    const country = document.createElement('li')
    country.append(`User country is = ${dataAboutUserIp.country}`)
    const regionName = document.createElement('li')
    regionName.append(`User regionName is = ${dataAboutUserIp.regionName}`)
    const city = document.createElement('li')
    city.append(`User city is = ${dataAboutUserIp.city}`)
    const area = document.createElement('li')
    area.append(`User area is = ${dataAboutUserIp.area}`)

    list.append(timezone, country, regionName, city, area);
    document.querySelector('.button').after(list)
}

    const button = document.createElement('div')
    button.classList.add('button')
    button.innerHTML = 'Знайти по IP'
    button.addEventListener('click', handleClick)


    const boxList = document.body
    boxList.appendChild(button);


