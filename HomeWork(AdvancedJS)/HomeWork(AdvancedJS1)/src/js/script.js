class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this.n_ame = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        this.n_ame = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

let newProgrammer = new Employee('Oleg', 22, 200);
let newProgrammerDima = new Programmer('Dima', 27, 300, ['spanish', 'english'])
let newEmployeeOksana = new Programmer('Oksana', 27, 400, ['spanish', 'english'])

console.log(newProgrammer);
console.log(newProgrammerDima);
console.log(newEmployeeOksana);


// 1. Коли ми зчитуємо якусь властивість об’єкта object, але її не має, JavaScript автоматично бере її з прототипу.
//     В програмуванні це називається “успадкування через прототипи”.

// 2. У конструкторі ключове слово super()використовується як функція, що викликає батьківський конструктор.
//     Потрібно викликати до першого звернення до ключового слова в телеконструкторі this. Ключове слово superтакож
// може бути використано для виклику функцій батьківського об'єкта.