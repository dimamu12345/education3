const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

let list = document.createElement('ul')
for (let i = 0; i < books.length; i++) {
    let element = document.createElement('li');
    element.append(books[i].author + books[i].name);
    if (books[i].author && books[i].name && books[i].price) {
        list.append(element)
    } else {
        if (!books[i].author) {
            console.error('No author')
        }
        if (!books[i].name) {
            console.error('No name')
        }
        if (!books[i].price) {
            console.error('No price')
        }
    }
}

const boxList = document.getElementById('root')
boxList.appendChild(list);