let allUsers = [];
let allPosts = [];

function makeRequest(method, url) {
    return new Promise(function (resolve, reject) {
        let xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.onload = function () {
           if (xhr.response){
               resolve(JSON.parse((xhr.response)))
           }else{
               resolve(true);
           }
        };
        console.log(xhr.response)
        xhr.onerror = function () {
            reject({
                status: xhr.status,
                statusText: xhr.statusText
            });
        };
        xhr.send();
    });
}

Promise.all([
    makeRequest("GET", "https://ajax.test-danit.com/api/json/users"),
    makeRequest("GET", "https://ajax.test-danit.com/api/json/posts"),
]).then(([users, posts]) => {
    renderData(users, posts);
});

function deletePost(postId) {
    const deleteURL = `https://ajax.test-danit.com/api/json/posts/${postId}`;
    makeRequest('DELETE', deleteURL).then(response => {
        document.getElementById(postId).remove();
    })
}



function renderData(users, posts) {

    let list = document.createElement('ul')
    for (let i = 0; i < posts.length; i++) {
        const post = posts[i];
        let postElement = document.createElement('li');
        postElement.id = post.id;
        const title = document.createElement('div'); // or span
        title.classList.add('post-title')
        title.append(post.title)

        const text = document.createElement('div');
        text.classList.add('user-text');
        text.append(post.body)
        postElement.append(title, text);

        for (let t = 0; t < users.length; t++) {
            const user = users[t];
            if (post.userId === user.id) {
                const data = document.createElement('div');
                data.classList.add('user-information');
                data.append(user.name + ',' + user.username + ',' + user.email);
                postElement.append(data);
                list.append(postElement);
            }
        }


        const buttonDelete = document.createElement('div')
        buttonDelete.classList.add('delete-button')
        buttonDelete.innerHTML = 'DELETE'
        buttonDelete.addEventListener('click', () => deletePost(post.id))


        list.append(postElement);
        postElement.append(buttonDelete);
    }


    const boxList = document.body
    boxList.appendChild(list);
}


