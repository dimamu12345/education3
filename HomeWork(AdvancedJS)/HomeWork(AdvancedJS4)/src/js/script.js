// AJAX (абревіатура від "Asynchronous Javascript And Xml") – технологія звернення до сервера без перезавантаження сторінок.
// Ajax поєднує кілька різних мов програмування, таких як HTML, CSS, JavaScript та інші. Насправді він працює за лаштунками,
//     щоб приймати запити від веб-браузера, надсилати їх на сервер та передавати результати назад до браузера.

let url = 'https://ajax.test-danit.com/api/swapi/films';


fetch(url)
    .then((response) => {
        return response.json();
    })
    .then((filmsArr) => {
        const shortFilmsInfo = filmsArr.map(item => {
            return {
                episodeId: "1."+"EpisodeId:" + item.episodeId + " ",
                name: "2." + "Name:" + item.name + " ",
                openingCrawl: "3." +  "OpeningCrawl:" + item.openingCrawl+ " "
            }
        })
        let list = document.createElement('ul')
        for (let i = 0; i < shortFilmsInfo.length; i++) {
            let element= document.createElement('li');
            element.append(shortFilmsInfo[i].episodeId + shortFilmsInfo[i].name + shortFilmsInfo[i].openingCrawl);
            if (shortFilmsInfo[i].episodeId && shortFilmsInfo[i].name && shortFilmsInfo[i].openingCrawl) {
                list.append(element);
            } else {
                console.error('Error');
            }
        }

        const boxList = document.body
        boxList.appendChild(list);

    });





