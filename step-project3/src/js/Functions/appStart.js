import createButton from "./createButton.js";
import filterUrg from "./filterUrg.js";
import {buttonCreateVisit} from "../DataStorage/buttons.js";
import {buttonEnter} from "../DataStorage/buttons.js";
import getCards from "../API/getCards.js";
import CardCardiologist from "../Classes/cardCardiologist.js";
import CardDentist from "../Classes/cardDentist.js";
import CardTherapist from "../Classes/cardTherapist.js";
import createElement from "./createElement.js";
import noItems from "../DataStorage/noItems.js";

function appStart() {
    const filterText = document.querySelector('#filter-text');
    const filterStatus = document.querySelector('#filter-status');
    const filterUrgency = document.querySelector('#filter-urgency');

    const token = localStorage.getItem('cardsToken');

    // filterUrg();
    
    if(token) {
        createButton(buttonCreateVisit);
        filterText.removeAttribute('disabled');
        filterStatus.removeAttribute('disabled');
        filterUrgency.removeAttribute('disabled');
        localStorage.setItem("CardsFiltered", JSON.stringify([]));
        
        getCards()
            .then(cards => {

                if (cards.length === 0) {
                    createElement(noItems);
                } else {
            
                    const cardsReversed = cards.reverse();
                    localStorage.setItem("Cards", JSON.stringify(cardsReversed));
                    localStorage.setItem("CardsFiltered", JSON.stringify(cardsReversed));

                    cardsReversed.forEach(card => {
                        if(card.doctor === "Кардіолог") {
                            new CardCardiologist(card).render('.cards');
                        } else if(card.doctor === "Дантист") {
                            new CardDentist(card).render('.cards');
                        } else {
                            new CardTherapist(card).render('.cards');
                        }
                    });
                }


            })
            .catch(e => console.error(e));
    } else {
        createElement(noItems);
        createButton(buttonEnter);
    }
}

export default appStart;