import deleteCard from "../API/deleteCard.js";
import removeArrElem from "../Functions/removeArrElem.js";
import createElement from "../Functions/createElement.js";
import noItems from "../DataStorage/noItems.js";

class Card {

    constructor({id, title, doctor, patientFullName, goal, description, urgency}) {

        this.id = id;
        this.title = title;
        this.doctor = doctor;
        this.patientFullName = patientFullName;
        this.goal = goal;
        this.description = description;
        this.urgency = urgency;
        this.cardDiv = document.createElement('div');
        this.titleH3 = document.createElement('h3');
        this.doctorP = document.createElement('p');
        this.patientFullNameP = document.createElement('p');
        this.cardHiddenDiv = document.createElement('div');
        this.patientGoalP = document.createElement('p');
        this.patientDescriptionP = document.createElement('p');
        this.patientUrgencyP = document.createElement('p');
        this.showMoreBtn = document.createElement('button');
        this.deleteBtn = document.createElement('button');
        this.editBtn = document.createElement('button');
    }

    drowElems(selector) {

        this.cardDiv.className = "card";
        this.titleH3.className = "card__title";
        this.patientFullNameP.className = "card__patient-name";
        this.doctorP.className = "card__doctor";
        this.cardHiddenDiv.className = "card__hidden"; 
        this.patientGoalP.className = "card__goal";
        this.patientDescriptionP.className = "card__desc";
        this.patientUrgencyP.className = "card__urgency";
        this.showMoreBtn.className = "card__showMoreBtn";
        this.deleteBtn.className = "card__deleteBtn";
        this.editBtn.className = "card__editBtn";

        this.titleH3.innerText = this.title;
        this.patientFullNameP.innerText = `ПІБ: ${this.patientFullName}`;
        this.doctorP.innerText = `Лікар: ${this.doctor}`;
        this.patientGoalP.innerText = `Мета візиту: ${this.goal}`;
        this.patientDescriptionP.innerText = `Короткий опис візиту: ${this.description}`;
        this.patientUrgencyP.innerText = `Терміновість: ${this.urgency}`;
        this.showMoreBtn.innerText = "Показати більше";
        this.deleteBtn.innerHTML = "<i class=\"fa fa-regular fa-close\"></i>";
        this.editBtn.innerHTML = "<i class=\"fa fa-solid fa-edit\"></i>";

        this.cardHiddenDiv.append(this.patientGoalP, this.patientDescriptionP, this.patientUrgencyP);

        this.cardDiv.append(this.titleH3, this.patientFullNameP, this.doctorP, this.showMoreBtn, this.cardHiddenDiv, this.editBtn, this.deleteBtn);

        document.querySelector(selector).insertAdjacentElement("afterbegin", this.cardDiv);
    }

    render(selector) {

       this.drowElems(selector);

       this.showMoreBtn.addEventListener('click', () => {

        if (this.cardHiddenDiv.className === "") {
            this.cardHiddenDiv.className = "card__hidden";
        } else {
            this.cardHiddenDiv.className = "";
        }
       });

       this.deleteBtn.addEventListener('click', () => {
        deleteCard(this.id)
            .then(res => {

                if(res.status) {
                    const cards = JSON.parse(localStorage.getItem('Cards'));
                    const pos = cards.map(e => e.id).indexOf(this.id);
                    const cardsRem = removeArrElem(cards, pos);
                    if (cardsRem.length === 0) { createElement(noItems); }
                    localStorage.setItem("Cards", JSON.stringify(cardsRem));
                    this.cardDiv.remove();
                }

            })
            .catch (e => console.error(e))
       });

    }

}

export default Card;