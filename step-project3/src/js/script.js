import appStart from "./Functions/appStart.js";
import testClearLocalStorage from "./Functions/testClearLocalStorage.js";
import testSetToken from "./Functions/testSetToken.js";
import removeArrElem from "./Functions/removeArrElem.js";

// Запускаємо web-застосунок
appStart();
// testClearLocalStorage();
// testSetToken();

// const cards = JSON.parse(localStorage.getItem('Cards'));
// const pos = cards.map(e => e.id).indexOf(132089);
// console.log(cards);
// console.log(pos);

// const cardsRem = removeArrElem(cards, pos);
// console.log(cardsRem);