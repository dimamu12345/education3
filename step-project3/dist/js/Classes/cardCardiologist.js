import Card from "./card.js";

class CardCardiologist extends Card {

    constructor({id, title, doctor, patientFullName, goal, description, urgency, age, bloodPressure, bmi, heartDiseases}) {

        super({id, title, doctor, patientFullName, goal, description, urgency});

        this.age = age;
        this.bloodPressure = bloodPressure;
        this.bmi = bmi;
        this.heartDiseases = heartDiseases;
        this.ageP = document.createElement('p');
        this.bloodPressureP = document.createElement('p');
        this.bmiP = document.createElement('p');
        this.heartDiseasesP = document.createElement('p');
    }

    drowElems(selector) {

        super.drowElems(selector);

        this.ageP.className = "card__age";
        this.bloodPressureP.className = "card__bloodPressure";
        this.bmiP.className = "card__bmi";
        this.heartDiseasesP.className = "card__heartDiseases";

        this.ageP.innerText = `Вік пацієнта: ${this.age} років.`;
        this.bloodPressureP.innerText = `Звичайний тиск пацієнта: ${this.bloodPressure}.`;
        this.bmiP.innerText = `Індекс маси тіла пацієнта: ${this.bmi}.`;
        this.heartDiseasesP.innerText = `Перенесені сердцево-судинні захворювання: ${this.heartDiseases}.`;

        this.cardHiddenDiv.append(this.ageP, this.bloodPressureP, this.bmiP, this.heartDiseasesP);

    }

    render(selector) {

        super.render(selector);

       this.drowElems(selector);



    }

}

export default CardCardiologist;