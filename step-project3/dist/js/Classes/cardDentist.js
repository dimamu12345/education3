import Card from "./card.js";

class CardDentist extends Card {

    constructor({id, title, doctor, patientFullName, goal, description, urgency, date}) {

        super({id, title, doctor, patientFullName, goal, description, urgency});

        this.date = date;
        this.dateArr = date.split("-");
        this.dateP = document.createElement('p');

    }

    drowElems(selector) {

        super.drowElems(selector);

        this.dateP.className = "card__date";

        this.dateP.innerText = `Дата останнього візиту: ${this.dateArr[2]}.${this.dateArr[1]}.${this.dateArr[0]}р.`;

        this.cardHiddenDiv.append(this.dateP);

    }

    render(selector) {

        super.render(selector);

       this.drowElems(selector);



    }

}

export default CardDentist;