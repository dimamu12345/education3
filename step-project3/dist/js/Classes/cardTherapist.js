import Card from "./card.js";

class CardTherapist extends Card {

    constructor({id, title, doctor, patientFullName, goal, description, urgency, age}) {

        super({id, title, doctor, patientFullName, goal, description, urgency});

        this.age = age;
        this.ageP = document.createElement('p');

    }

    drowElems(selector) {

        super.drowElems(selector);

        this.ageP.className = "card__age";

        this.ageP.innerText = `Вік пацієнта: ${this.age} років.`;

        this.cardHiddenDiv.append(this.ageP);

    }

    render(selector) {

        super.render(selector);

       this.drowElems(selector);



    }

}

export default CardTherapist;