import ModalCreateVisit from "../Classes/modalCreateVisit.js";

function buttonModalCreateVisitHandler() {
    new ModalCreateVisit().render('.wrapper');
}

export default buttonModalCreateVisitHandler;