import showCards from "./showCards.js";

function filterUrg() {

    const filterUrgency = document.querySelector('#filter-urgency');
    const сards = JSON.parse(localStorage.getItem("Cards"));
    const filteredCards = JSON.parse(localStorage.getItem("CardsFiltered"));

    filterUrgency.addEventListener('change', () => {
        let filterUrgencyValue = filterUrgency.value;
        let flag, result;

        if (filterUrgencyValue === "Low") { flag = "Звичайна" }
        if (filterUrgencyValue === "Normal") { flag = "Пріорітетна" }
        if (filterUrgencyValue === "High") { flag = "Невідкладна" }
        if (filterUrgencyValue === "All") { flag = false }

        if(flag) {
            result = filteredCards.filter(card => card.urgency === flag);
            localStorage.setItem('CardsFiltered', JSON.stringify(result));
            // showCards();
        } else {
            localStorage.setItem('CardsFiltered', JSON.stringify(сards));
    
        }

        showCards();
    });
    
}

export default filterUrg;