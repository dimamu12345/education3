"use strict";

const newBgName = 'new-background';
const localStorageKey = 'backgroundClass';

if (localStorage.getItem(localStorageKey)) {
    document.body.classList.add(newBgName);
}

let newButton = document.getElementById('btn-style')

function newStyle() {
    if (localStorage.getItem(localStorageKey)) {
        document.body.classList.remove(newBgName);
        localStorage.removeItem(localStorageKey)
    } else {
        document.body.classList.add(newBgName);
        localStorage.setItem(localStorageKey, newBgName);
    }

}

newButton.addEventListener('click', newStyle)
