"use strict";
let activeIndex = 0;
const activeClassName = 'hidden';
let images = document.getElementsByClassName('image-to-show')
let intervalId;

function startInterval() {
    intervalId = setInterval(() => {
        const prevIndex = activeIndex - 1;
        if (prevIndex >= 0) {
            images[prevIndex].classList.add(activeClassName)
        }
        if (images[activeIndex]) {
            images[activeIndex].classList.remove(activeClassName)
        }
        activeIndex = activeIndex + 1;
        if (activeIndex > images.length) {
            activeIndex = 0;
        }
    }, 3000)
}

let firstButton = document.getElementById('first-button')
firstButton.addEventListener("click", stopInterval);

function stopInterval() {
    clearInterval(intervalId);
}

let secondButton = document.getElementById("second-button")
secondButton.addEventListener("click", startInterval);

startInterval();

// 1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
//      SetTimeout дозволяє викликати функцію один раз через певний проміжок часу. setInterval дозволяє викликати
//      функцію регулярно, повторюючи виклик через певний проміжок часу.
// 2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
//      Функція зворотного виклику спрацьовує «швидко або якнайшвидше.
// 3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
//      Для того щоб не навантажувати код і сторінку