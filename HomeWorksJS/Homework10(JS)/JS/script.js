"use strict";

let activeIndex = 0;

const activeClassName = 'active';

const tabs = document.getElementsByClassName("tabs-title");
const tabsContent = document.querySelectorAll(".item");

for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener("click", (event) => {
        if (typeof activeIndex === 'number') {
            tabs[activeIndex].classList.remove(activeClassName);
            tabsContent[activeIndex].classList.remove(activeClassName);
        }
        tabs[i].classList.add(activeClassName);
        tabsContent[i].classList.add(activeClassName);
        activeIndex = i;
    });
}
