"use strict";

let highlightedItem = null;

document.addEventListener('keydown', (event) => {
    switch (event.code) {
        case 'Enter': {
            highlightHtmlElementById('Enter');
            break;
        }
        case 'KeyS': {
            highlightHtmlElementById('KeyS');
            break;
        }
        case 'KeyE': {
            highlightHtmlElementById('KeyE');
            break;
        }
        case 'KeyO': {
            highlightHtmlElementById('KeyO');
            break;
        }
        case 'KeyN': {
            highlightHtmlElementById('KeyN');
            break;
        }
        case 'KeyL': {
            highlightHtmlElementById('KeyL');
            break;
        }
        case 'KeyZ': {
            highlightHtmlElementById('KeyZ');
            break;
        }
        default:
            break;
    }
});

function highlightHtmlElementById(id) {
    if (highlightedItem) {
        highlightedItem.classList.remove('highlight')
    }

    highlightedItem = document.getElementById(id);
    highlightedItem.classList.add('highlight');
}
