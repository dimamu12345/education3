"use strict";

function newFunction(arr, parent=document.body) {
    const newParent = document.createElement("ul")
    for (let i = 0; i < arr.length; i++){
        const newLi =  document.createElement("li")
        newLi.append(arr[i]);
        newParent.append(newLi);
    }
    parent.append(newParent);
}

newFunction(
    ['hello','world','Kiev', 'Kharkiv', 'Odessa', 'Lviv']
);


// 1.Опишіть, як можна створити новий HTML тег на сторінці.
//    document.createElement(tag) - створює новий елемент з заданим тегом
//    document.createTextNode(text) - створює новий текстовий вузол з заданим текстом:
//
// 2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
//      Перший параметр insertAdjacentHTML це position. визначає позицію елемента, що додається щодо елемента, що
//      викликав метод. Повинно відповідати одному з наступних значень: 'beforebegin', 'afterbegin', 'beforeend',
//      'afterend'.
// 3.Як можна видалити елемент зі сторінки?
//       .remove()