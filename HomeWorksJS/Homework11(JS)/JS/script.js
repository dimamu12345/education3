"use strict";

const activeClassName = 'displayHidden';

let icon = document.getElementsByClassName('fa-eye');
let iconSlash = document.getElementsByClassName('fa-eye-slash');
let passwordInputs = document.getElementsByClassName('passwordHidden');

const changeInputType = (inputElement, inputType) => {
    inputElement.setAttribute('type', inputType)
}

icon[0].addEventListener("click", () => {
    icon[0].classList.add(activeClassName);
    iconSlash[0].classList.remove(activeClassName);
    changeInputType(passwordInputs[0], 'password');
})
iconSlash[0].addEventListener("click", () => {
    iconSlash[0].classList.add(activeClassName);
    icon[0].classList.remove(activeClassName);
    changeInputType(passwordInputs[0], 'text');
})

icon[1].addEventListener("click", () => {
    icon[1].classList.add(activeClassName);
    iconSlash[1].classList.remove(activeClassName);
    changeInputType(passwordInputs[1], 'password');
})
iconSlash[1].addEventListener("click", () => {
    iconSlash[1].classList.add(activeClassName);
    icon[1].classList.remove(activeClassName);
    changeInputType(passwordInputs[1], 'text');
})

let button = document.getElementsByClassName('btn')[0]

button.addEventListener("click", () => {
    if (
        passwordInputs[0].value && passwordInputs[1].value &&
        (passwordInputs[0].value === passwordInputs[1].value)
    ) {
        alert('You are welcome')
    } else {
        alert('Потрібно ввести однакові значення')
    }
})

